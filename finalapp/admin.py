from django.contrib import admin
from finalapp.models import Student,category,Product,registration

admin.site.site_header = "Finalproject | Website Name"

class StudentAdmin(admin.ModelAdmin):
    fields= ['name','website','course','fee','is_approved']
    list_display = ['id','name','website','course','fee','is_approved','registred_on']
    list_filter = ['name','email']
    list_editable = ['name']
    search_fields = ['name','course']

admin.site.register(Student,StudentAdmin)
admin.site.register(category)
admin.site.register(Product)
admin.site.register(registration)
