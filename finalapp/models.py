from django.db import models
from django.contrib.auth.models import User

class registration(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    contact = models.IntegerField()
    profile_pic = models.ImageField(upload_to="profiles/%Y/%m/%d")

    def __str__(self):
        return self.user.username

class Student(models.Model):
    c = (
        ('BT','B.Tech'),('BCA','BCA')
    )
    name = models.CharField(max_length=250)
    email = models.EmailField()
    contact = models.IntegerField()
    course = models.CharField(max_length=250,choices=c)
    fee = models.DecimalField(decimal_places=2,max_digits=10)
    website = models.URLField(blank=True)
    is_approved = models.BooleanField(default=True)
    address= models.TextField()
    dob = models.DateField()
    registred_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.contact)    

    class Meta:
        verbose_name_plural="Student Table"

class category(models.Model):
    c_name = models.CharField(max_length=200)
    added_on = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.c_name
class Product(models.Model):
    name = models.CharField(max_length=250)
    cat = models.ForeignKey(category,on_delete=models.CASCADE,related_name="products")
    m_price=models.DecimalField(decimal_places=2,max_digits=10)
    s_price=models.DecimalField(decimal_places=2,max_digits=10)
    image = models.ImageField(upload_to="products/%Y/%m/%d")
    description = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
    