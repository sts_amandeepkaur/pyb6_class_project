from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from finalapp.models import category, Product, registration
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
import datetime
cats = category.objects.all().order_by('c_name')
# products = Product.objects.all().order_by('-id')[:3]

def index(request):   
    if "userid" in request.COOKIES:
        uid = request.COOKIES['userid']
        user = User.objects.get(id=uid)
        login(request,user)
        return HttpResponseRedirect('/dashboard')
    if request.method=="POST":
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username,password=password)
        if user:
            if user.is_superuser:    
                login(request,user)
                return HttpResponseRedirect('/admin/')
            elif user.is_active:
                login(request,user)
                response = HttpResponseRedirect('/dashboard/')
                if "remember" in request.POST:
                    response.set_cookie("userid",user.id)
                    response.set_cookie("password",password)
                    response.set_cookie("created_on",datetime.datetime.now())
                return response
            else:
                return render(request,'index.html',{'c':cats,'st':'You are not allowed'})    
        else:
            return render(request,'index.html',{'c':cats,'st':'Invalid Username or Password'})
    return render(request,'index.html',{'c':cats})
 
def uslogout(request):
    logout(request)
    response = HttpResponseRedirect('/')
    response.delete_cookie("userid")
    response.delete_cookie("created_on")
    response.delete_cookie("password")
    return  response

def aboutpage(r):
    if r.method=="POST":
        name = r.POST['pname']
        mprice = r.POST['mprice']
        sprice = r.POST['oprice'] 
        desc = r.POST['desc']
        c_id = r.POST['categories']
        cat_obj = category.objects.get(id=c_id)
        if "add" in r.POST:
            data = Product(name=name,cat=cat_obj,m_price=mprice,s_price=sprice,description=desc)
            data.save()
            if 'pimg' in r.FILES:
                img = r.FILES['pimg']
                data.image = img
                data.save()
            st = "{} added successfully!!".format(data.name)
            return render(r,'about.html',{'c':cats,'status':st})
        if "update" in r.POST:
            uid  = r.GET['upd']
            prd = Product.objects.get(id=uid)
            prd.name = name
            prd.cat = cat_obj
            prd.m_price = mprice
            prd.s_price = sprice
            prd.description = desc
            prd.save()
            if "pimg" in r.FILES:
                img = r.FILES['pimg']
                prd.image = img
                prd.save()
            st = "{} updated successfully!!".format(prd.name)
            return render(r,'about.html',{'c':cats,'status':st})
    if "upd" in r.GET:
        up_id = r.GET['upd']
        prd = Product.objects.get(id=up_id)
        return render(r,'about.html',{'c':cats,'p':prd})
    return render(r,'about.html',{'c':cats})
# @login_required
def view_products(request):
    products = Product.objects.all().order_by('-id')
    if "del" in request.GET:
        id = request.GET['del']
        prd = Product.objects.get(id=id)
        prd.delete()
        return HttpResponseRedirect('/contact')
    if 'search_item' in request.GET:
        search_item = request.GET['search_item']
        products = Product.objects.filter(name__contains=search_item)|Product.objects.filter(cat__c_name__contains=search_item)
        return render(request,'contact.html',{'products':products,'total':len(products),'c':cats})
    if 'id' in request.GET:
        c_id = request.GET['id']
        products = Product.objects.filter(cat__id=c_id)
        return render(request,'contact.html',{'products':products,'total':len(products),'c':cats})
    return render(request,'contact.html',{'products':products,'total':len(products),'c':cats})

def signup(request):
    if request.method=="POST":
        st=''
        fs = request.POST['first']
        ls = request.POST['last']
        usern = request.POST['un']
        em = request.POST['em']
        pas = request.POST['pas']
        con = request.POST['con']
        check = User.objects.filter(username=usern)
        if len(check)==0:
            user = User.objects.create_user(usern,em,pas)
            user.first_name = fs 
            user.last_name = ls 
            user.is_staff=True
            user.save()

            rg = registration(user=user,contact=con)
            rg.save()
            if "pic" in request.FILES:
                pic = request.FILES['pic']
                rg.profile_pic = pic
                rg.save()
            st = "{} registred Successfully".format(user.first_name)
        else:
            st = "A user with this username already exists!"
        return render(request,'signup.html',{'status':st})
    return render(request,'signup.html')

def checkUserExist(request):
    un = request.GET['usern']
    ch = User.objects.filter(username=un)
    if len(ch)>0:
        return HttpResponse(1)
    else:
        return HttpResponse(0)

def dashboard(request):
    profile = registration.objects.get(user=request.user)
    return render(request,'dashboard.html',{'profile':profile})

def testfun(request):
    return HttpResponse("IT IS WORKING")