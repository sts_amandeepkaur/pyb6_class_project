"""finalproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from finalapp import views
from django.conf import settings
from django.conf.urls.static import static

from rest_framework import routers,serializers,viewsets
from finalapp.models import Product ,category

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['id','name','cat','m_price','s_price','image','description','added_on']

class catSerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True)
    class Meta:
        model = category
        fields = ['id','c_name','products','added_on']

class ProductViewset(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class catViewset(viewsets.ModelViewSet):
    queryset = category.objects.all()
    serializer_class = catSerializer

router = routers.DefaultRouter()
router.register('products',ProductViewset)
router.register('cats',catViewset)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.index,name="home"),
    path('about/',views.aboutpage,name="about"),
    path('contact/',views.view_products,name="con"),
    path('signup/',views.signup,name="signup"),
    path('uslogout/',views.uslogout,name="uslogout"),
    path('dashboard/',views.dashboard,name="dashboard"),
    path('testfun/',views.testfun),
    path('checkUserExist/',views.checkUserExist,name="checkUserExist"),
    path('api/',include(router.urls)),
    path('api-auth',include('rest_framework.urls'),name="rest_framework")
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
